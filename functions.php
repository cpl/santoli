<?php
// function theme_styling() {
// 	wp_enqueue_style( 'santoli', get_template_directory_uri() . '/style.css' ); // main style.css
// }
// add_action('wp_head', 'theme_styling', 4);


if ( ! function_exists( 'santoli_theme_support' ) ) :
    function santoli_theme_support()  {

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		// Post thumbnails
		add_theme_support( 'post-thumbnails' );

		// Set post thumbnail size.
		set_post_thumbnail_size( 400, 300 );

		// HTML5 semantic markup
		add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

		// Alignwide and alignfull classes in the block editor
		// add_theme_support( 'align-wide' );

		add_theme_support( 'wp-block-styles' );

		add_theme_support( 'editor-styles');

		// add_theme_support( 'disable-custom-font-sizes' );

		//add_theme_support( 'disable-custom-gradients' );

		// add_theme_support( 'custom-units', 'rem', 'em' );


	}
endif;
	add_action( 'after_setup_theme', 'santoli_theme_support' );
